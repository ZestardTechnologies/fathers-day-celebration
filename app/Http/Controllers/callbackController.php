<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\AddProductSetting;
use App\block_config;
use App\Symbol;
use App\ShopCurrency;
use App\app_config;

class callbackController extends Controller {

    public function index(Request $request) {
        $sh = App::make('ShopifyAPI');

        $app_settings = DB::table('appsettings')->where('id', 1)->first();

        if (!empty($_GET['shop'])) {
            $shop = $_GET['shop'];
            $select_store = DB::table('usersettings')->where('store_name', $_GET['shop'])->get();

            if (count($select_store) > 0) {
                session(['shop' => $shop]);
                return redirect()->route('dashboard', ['shop' => $shop]);
                //Remove coment for the Payment method
//                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
//                $id = $select_store[0]->charge_id;
//                $url = 'admin/application_charges/' . $id . '.json';
//                //dd($url);
//                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
//                //dd($charge);
//                $charge_id = $select_store[0]->charge_id;
//                $charge_status = $select_store[0]->status;
//                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
//                    session(['shop' => $shop]);
//                    return redirect()->route('dashboard');
//                } else {
//                    return redirect()->route('payment_process');
//                }
            } else {
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);

                $permission_url = $sh->installURL(['permissions' => array('read_script_tags', 'write_script_tags', 'read_themes', 'write_content', 'write_themes'), 'redirect' => $app_settings->redirect_url]);
                return redirect($permission_url);
            }
        }
    }

    public function dashboard(Request $request) {
        $shop = session('shop');
        //dd($shop);
        if(empty($shop))
		{
			$shop = $_GET['shop'];
			
			session(['shop' => $shop]);			
        }
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop_model = new ShopModel;
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $shop_data = "";
        $shop_data = app_config::where('store_id', $shop_find->id)->first();
        $short_code_status = 0;

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        
        //api call for get theme info
        $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);        
        foreach ($theme->themes as $themeData) {
            if ($themeData->role == 'main') {
                $theme_id = $themeData->id;

                $theme_template = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json?asset[key]=layout/theme.liquid&theme_id=' . $theme_id, 'METHOD' => 'GET']);                
                $old_str = $theme_template->asset->value;
                $is_exist = "{% include 'father-day-celebration' %}";
                if (strpos($old_str, $is_exist) === false) {
                    $short_code_status = 1;
                    // $str_to_insert = " {% include 'father-day-celebration' %} ";
                    // $find = "{% section 'footer' %}";
                    // $pos = strpos($old_str, $find);
                    
                        
                    // if (strpos($old_str, $find) === false) {
                    //     $find = "{%- if template.name == 'index' -%}";
                    //     $pos = strpos($old_str, $find) - 1;                
                    // }else{
                    //     $pos = strpos($old_str, $find) + 25;
                    // }
                    // //$newstr = substr_replace($old_str, $str_to_insert, $pos, 0);
                    // //$call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'layout/theme.liquid', 'value' => $newstr]]]);

                } 
            }
        }

        //        $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);
        //        foreach($theme->themes as $themeData){
        //            if($themeData->role == 'main'){
        //                $theme_id = $themeData->id;
        //
          //                $value= "{% include 'christmas' %}";
        //                //echo '<pre>'; print_r($sh);die;
        //                //api call for adding shortcode in theme.liquid by theme assets
        //                $themeShortcode = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT',
        //                    'DATA' => ['asset' => ['key' => 'layout/theme.liquid', 'value' => $value] ] ]);
        //                //echo '<pre>'; print_r($themeShortcode);die;
        //
          //            }
        //        }

        return view('dashboard', ['shopdomain'=> $shop_find->store_name, 'store_record' => $shop_data, 'active' => 'globalset', 'short_code_status' => $short_code_status]);
    }

    public function payment_method(Request $request) {
        $shop = session('shop');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        if (count($select_store) > 0) {
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

            $charge_id = $select_store[0]->charge_id;
            $url = 'admin/application_charges/' . $charge_id . '.json';
            $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
            if (count($charge) > 0) {
                if ($charge->application_charge->status == "pending") {
                    echo '<script>window.top.location.href="' . $charge->application_charge->confirmation_url . '"</script>';
                } elseif ($charge->application_charge->status == "declined" || $charge->application_charge->status == "expired") {
                    //creating the new Recuring charge after declined app
                    $url = 'https://' . $shop . '/admin/application_charges.json';
                    $charge = $sh->call([
                        'URL' => $url,
                        'METHOD' => 'POST',
                        'DATA' => array(
                            'application_charge' => array(
                                'name' => "Father's Day Celebration",
                                'price' => 7.00,
                                'return_url' => url('payment_success'),
                                //'test' => true
                            )
                        )
                            ], false);

                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string) $charge->application_charge->id, 'api_client_id' => $charge->application_charge->api_client_id, 'price' => $charge->application_charge->price, 'status' => $charge->application_charge->status, 'billing_on' => '', 'payment_created_at' => $charge->application_charge->created_at, 'activated_on' => '', 'trial_ends_on' => '', 'cancelled_on' => '', 'trial_days' => '', 'decorated_return_url' => $charge->application_charge->decorated_return_url, 'confirmation_url' => $charge->application_charge->confirmation_url, 'domain' => $shop]);

                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->application_charge->confirmation_url . '"</script>';
                } elseif ($charge->application_charge->status == "accepted") {

                    $active_url = '/admin/application_charges/' . $charge_id . '/activate.json';
                    $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
                    $Activatecharge_array = get_object_vars($Activate_charge);
                    $active_status = $Activatecharge_array['application_charge']->status;
                    $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
                    return redirect()->route('dashboard');
                }
            }
        }
    }

    public function payment_compelete(Request $request) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $charge_id = $_GET['charge_id'];
        $url = 'admin/application_charges/#{' . $charge_id . '}.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET',]);
        $status = $charge->application_charges[0]->status;

        $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $status]);

        if ($status == "accepted") {
            $active_url = '/admin/application_charges/' . $charge_id . '/activate.json';
            $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
            $Activatecharge_array = get_object_vars($Activate_charge);
            $active_status = $Activatecharge_array['application_charge']->status;
            $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
            return redirect()->route('dashboard');
        } elseif ($status == "declined") {
            echo '<script>window.top.location.href="https://' . $shop . '/admin/apps"</script>';
        }
    }

    public function Currency(Request $request) {
        $shop = session('shop');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop_model = new ShopModel;
        $shop_find = ShopModel::where('store_name', $shop)->first();
        $currency_details = Symbol::where('id', $request->currency)->first();
        $currency_update = ShopCurrency::where('shop_id', $shop_find->id)->first();
        $currency_update->currency_code = $currency_details->currency_code;
        $currency_update->save();
        $currency_updated = ShopCurrency::where('shop_id', $shop_find->id)->first();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $store_details = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
        $currency_format = Symbol::where('currency_code', $store_details->shop->currency)->first();
        $app_currencyformat = ShopCurrency::where('shop_id', $shop_find->id)->first();
        $currencyformat_app = Symbol::where('currency_code', $app_currencyformat->currency_code)->first();
        $all_currency = Symbol::all();
        $new_format = Symbol::where('currency_code', $currency_updated->currency_code)->first();
        return view('dashboard', ['store_detail' => $currency_format, 'app_currency' => $currencyformat_app, 'currency' => $all_currency, 'currency_updated' => $new_format, 'shop_details' => $shop_find]);
    }

    public function validatefrontend(Request $request) {
        $id = $request->id;
        $shop_find = ShopModel::where('store_encrypt', $id)->first();
        if ($shop_find->status == "active") {
            return view('search');
        } else {
            return "Page Not Found";
        }
    }

    public function redirect(Request $request) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        if (!empty($request->input('shop')) && !empty($request->input('code'))) {
            $shop = $request->input('shop'); //shop name
            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
            if (count($select_store) > 0) {
                 session(['shop' => $shop]);
                 return redirect()->route('dashboard');
//                //Remove coment for the Payment method
//                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
//                $id = $select_store[0]->charge_id;
//                $url = 'admin/application_charges/' . $id . '.json';
//                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
//                $charge_id = $select_store[0]->charge_id;
//                $charge_status = $select_store[0]->status;
//                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
//                    session(['shop' => $shop]);
//                    return redirect()->route('dashboard');
//                } else {
//                    return redirect()->route('payment_process');
//                }
            }
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
            try {
                $verify = $sh->verifyRequest($request->all());
                if ($verify) {
                    $code = $request->input('code');
                    $accessToken = $sh->getAccessToken($code);

                    DB::table('usersettings')->insert(['access_token' => $accessToken, 'store_name' => $shop, 'store_encrypt' => ""]);
                    $shop_find = ShopModel::where('store_name', $shop)->first();
                    $shop_id = $shop_find->id;

                    $christmas_encrypt = crypt($shop_id, "ze");
                    $finaly_encrypt = str_replace(['/', '.'], "Z", $christmas_encrypt);

                    $update_encrypt = ShopModel::where('id', $shop_id)->update(['store_encrypt' => $finaly_encrypt]);

                    //dd($shop_find->store_encrypt);
                    $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

                    //api call for creating the app script tag
                    //$script = $sh->call(['URL' => '/admin/script_tags.json', 'METHOD' => 'POST', 'DATA' => ['script_tag' => ['event' => 'onload', 'src' => config('app.url') . 'public/js/father-day-celebration.js']]]);

                    //for creating the uninstall webhook
                    $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json';
                    $webhookData = [
                        'webhook' => [
                            'topic' => 'app/uninstalled',
                            'address' => config('app.url') . 'uninstall.php',
                            'format' => 'json'
                        ]
                    ];
                    $uninstall = $sh->appUninstallHook($accessToken, $url, $webhookData);

                    //api call for get theme info
                    $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);
                    foreach ($theme->themes as $themeData) {
                        if ($themeData->role == 'main') {

                            $snippets_arguments = ['id' => $finaly_encrypt];
                            $theme_id = $themeData->id;
                            $view = (string) View('snippets', $snippets_arguments);

                            //api call for creating snippets
                            $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/father-day-celebration.liquid', 'value' => $view]]]);

                            $theme_template = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json?asset[key]=layout/theme.liquid&theme_id=' . $theme_id, 'METHOD' => 'GET']);                
                            $old_str = $theme_template->asset->value;
                            $is_exist = "{% include 'father-day-celebration' %}";
                            if (strpos($old_str, $is_exist) === false) {
                                $str_to_insert = " {% include 'father-day-celebration' %} ";
                                $find = "{% section 'footer' %}";
                                $pos = strpos($old_str, $find);

                                if (strpos($old_str, $find) === false) {
                                    $find = "{%- if template.name == 'index' -%}";
                                    $pos = strpos($old_str, $find) - 1;                
                                }else{
                                    $pos = strpos($old_str, $find) + 25;
                                }
                                $newstr = substr_replace($old_str, $str_to_insert, $pos, 0);
                                $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'layout/theme.liquid', 'value' => $newstr]]]);

                            } 
                        }
                    }
                


                    session(['shop' => $shop]);
                    //return redirect('dashboard');
                    //creating the Recuring charge for app
//                    if ($shop == "shopify-dev-app.myshopify.com") {
//                        $url = 'https://' . $shop . '/admin/application_charges.json';
//                        $charge = $sh->call([
//                            'URL' => $url,
//                            'METHOD' => 'POST',
//                            'DATA' => array(
//                                'application_charge' => array(
//                                    'name' => "Father's Day Celebration",
//                                    'price' => 7.00,
//                                    'return_url' => url('payment_success'),
//                                    'capped_amount' => 20,
//                                    'terms' => 'Terms & Condition Applied',
//                                    'test' => true
//                                )
//                            )
//                                ], false);
//                    } else {
//                    $url = 'https://' . $shop . '/admin/application_charges.json';
//                    $charge = $sh->call([
//                        'URL' => $url,
//                        'METHOD' => 'POST',
//                        'DATA' => array(
//                            'application_charge' => array(
//                                'name' => "Father's Day Celebration",
//                                'price' => 7.00,
//                                'return_url' => url('payment_success'),
//                                'capped_amount' => 20,
//                                'terms' => 'Terms & Condition Applied',
//                                //'test' => true
//                            )
//                        )
//                            ], false);
//                }   
                    
                   // $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string) $charge->application_charge->id, 'api_client_id' => $charge->application_charge->api_client_id, 'price' => $charge->application_charge->price, 'status' => $charge->application_charge->status, 'billing_on' => '', 'payment_created_at' => $charge->application_charge->created_at, 'activated_on' => '', 'trial_ends_on' => '', 'cancelled_on' => '', 'trial_days' => '', 'decorated_return_url' => $charge->application_charge->decorated_return_url, 'confirmation_url' => $charge->application_charge->confirmation_url, 'domain' => $shop]);

                    $storeDetails = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
                    foreach ($storeDetails as $storeData) {
                        $email = $storeData->email;
                        $created_at = $storeData->created_at;
                        $phone = $storeData->phone;
                        $country = $storeData->country_code;
                        $owner = $storeData->shop_owner;
                        $plan = $storeData->plan_name;
                        $domain = $storeData->domain;
                    }
                    $create_charge = DB::table('masterdata')->insert(['email' => $email, 'created_at' => $created_at, 'phone' => $phone, 'country' => $country, 'owner' => $owner, 'plan' => $plan, 'domain' => $domain]);

                    $shopi_info = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);

                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    $msg = '<table>
                            <tr>
                                <th>Shop Name</th>
                                <td>' . $shopi_info->shop->name . '</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>' . $shopi_info->shop->email . '</td>
                            </tr>
                            <tr>
                                <th>Domain</th>
                                <td>' . $shopi_info->shop->domain . '</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td>' . $shopi_info->shop->phone . '</td>
                            </tr>
                            <tr>
                                <th>Shop Owner</th>
                                <td>' . $shopi_info->shop->shop_owner . '</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>' . $shopi_info->shop->country_name . '</td>
                            </tr>
                            <tr>
                                <th>Plan</th>
                                <td>' . $shopi_info->shop->plan_name . '</td>
                            </tr>
                          </table>';
                   $store_details = DB::table('development_stores')->where('dev_store_name', $shop)->first();
                       
                    if(count($store_details) <= 0){
                        mail("support@zestard.com", "Father's Day Celebration App Installed", $msg, $headers);                        
                    }
                   return redirect()->route('dashboard');
                    //redirecting to the Shopify payment page
                    //echo '<script>window.top.location.href="' . $charge->application_charge->confirmation_url . '"</script>';
                } else {
                    //Issue with data
                }
            } catch (Exception $e) {
                echo '<pre>Error: ' . $e->getMessage() . '</pre>';
            }
        }
    }

}
